<?php get_header();?>
	<section class="section-header">
		<div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
		  <div class="carousel-indicators">
		    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
		    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="1" aria-label="Slide 2"></button>
<!-- 		    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="2" aria-label="Slide 3"></button> -->
		  </div>
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img class="img-overlay-1" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="...">
		      <div class="mask" style="background-color: rgba(0,0,0,.6);">
				<div class="d-flex justify-content-end align-items-center text-white h-100">
					<div class="container">
						<h1 class="text-capitalize">bisnis adalah tentang membangun SDM<br>mengapa kita perlu mengembangkan SDM untuk menumbuhkan bisnis?</h1>
						<p class="text-capitalize fs-5">Masukkan Email Anda Di Bawah Ini Untuk Mempelajari Alasannya!</p>
						<div class="col-12 col-md-6 col-lg-4">
							<?php echo do_shortcode('[contact-form-7 id="9338892918916" title="Contact form 1"]'); ?>
							<!-- <input type="email" class="form-control form-control-lg mb-3 text-center" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Email Aktif Anda">
							<a href="#" class="btn btn-theme text-uppercase btn-theme-1" style="width: 100%">Subscribe</a> -->
						</div>
					</div>
				</div>
		      </div>
		    </div>
		    <div class="carousel-item">
		      <img class="img-overlay-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"alt="...">
		      <div class="mask" style="background-color: rgba(0,0,0,.6);">
				<div class="d-flex justify-content-end align-items-center text-white h-100">
					<div class="container">
						<h1>Pilihan Metode Pelatihan</h1>
						<p>Kami Menawarkan 3 Metode Pelatihan Yaitu :</p>
						<div class="d-flex">
							<i class="far fa-check flex-shrink-0 me-3 fs-1"></i>
							<div>
								<p class="fs-5 fw-bold mb-0">E-Learning Program</p>
								<p>Menggunakan sistem elektronik atau komputer</p>
							</div>
						</div>
						<div class="d-flex">
							<i class="far fa-check flex-shrink-0 me-3 fs-1"></i>
							<div>
								<p class="fs-5 fw-bold mb-0">Distance Learning Program</p>
								<p>Sistem telekomunikasi interaktif.</p>
							</div>
						</div>
						<div class="d-flex">
							<i class="far fa-check flex-shrink-0 me-3 fs-1"></i>
							<div>
								<p class="fs-5 fw-bold mb-0">Face to Face Training</p>
								<p>Dilakukan secara tatap muka.</p>
							</div>
						</div>
					</div>
				</div>
		      </div>
		    </div>
<!-- 		    <div class="carousel-item">
		      <img class="img-overlay-3" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="...">
		      <div class="mask" style="background-color: rgba(0,0,0,.6);">
				<div class="d-flex justify-content-end align-items-center text-white h-100">
					<div class="container text-center">
						<h1>labore et dolore magna aliqua</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>
					</div>
				</div>
		      </div>
		    </div> -->
		  </div>
<!-- 		  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade"  data-bs-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="visually-hidden">Previous</span>
		  </button>
		  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade"  data-bs-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="visually-hidden">Next</span>
		  </button> -->
		</div>
	</section>
	<section class="section-about bg-light">
		<div class="bg-white py-5 mx-lg-3 rounded-2 shadow-sm about-wrap" data-aos="fade-up" data-aos-duration="100">
			<div class="container" data-aos="fade">
				<div class="d-block d-md-flex text-center text-md-start">
				<img width="110" height="110" class="flex-shrink-0 me-0 me-md-4" src="<?php bloginfo('template_url'); ?>/assets/images/logo/logo-rounded.svg">
				<div class="mt-4 mt-md-0">
					<h1 class="text-uppercase position-relative m-0" style="top: -10px">Tentang Bisa</h1>
					<p class="mb-0 position-relative" style="top: -10px">Selamat datang di BISA INDONESIA. Kami merupakan Perusahaan Konsultan Pengembangan Sumber Daya Manusia. Kami juga berperan sebagai Pusat Pendidikan, pelatihan, dan Tempat uji kompetensi yang bekerja sama dengan berbagai LSP (Lembaga Sertifikasi Profesi) di bawah pengawasan BNSP (Badan Nasional Sertifikasi Profesi) untuk berbagai skema profesi.</p>
				</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-why pb-5 bg-light">
		<div class="container">
			<div class="heading text-center mb-5">
				<h1 class="text-uppercase" data-aos="fade">Mengapa Kami?</h1>
			</div>
			<div class="row mb-4">
				<?php
		        $args = array( 
		            'post_type' => 'post', 
		            'cat' => '8', 
		            'order'   => 'ASC',
		        );
		        $query = new WP_Query( $args ); //query the categories

				if ($query->have_posts()) {
				while ($query->have_posts()) {
				$query->the_post(); ?>

				<div class="col-12 col-lg-6 ">
					<div class="card card-why border-0 rounded-2 shadow-sm bg-white mb-lg-0" data-aos="fade-up">
						<div class="icon-why-wrap card-body text-center">
							<p><?php echo the_content(); ?></p>
					  	</div>
					</div>
				</div>

		        <?php } ?>
	            <?php }  else {
		            // not found article ?>
		            <div class="post-preview">
		                <h2 class="post-title">Article Not Found!!!</h2>
		            </div>
			    <?php } ?>
			</div>

			<div class="row">
				<?php
		        $args = array( 
		            'post_type' => 'post', 
		            'cat' => '9', 
		            'order'   => 'ASC',
		        );
		        $query = new WP_Query( $args ); //query the categories;
				if ($query->have_posts()) {
				while ($query->have_posts()) {
				$query->the_post(); ?>

				<div class="col-12 col-lg-6">
					<div class="d-flex my-2 why-left" data-aos="zoom-in">
						<p><?php echo the_content(); ?></p>
					</div>
				</div>

		        <?php } ?>
	            <?php }  else {
		            // not found article ?>
		            <div class="post-preview">
		                <h2 class="post-title">Article Not Found!!!</h2>
		            </div>
			    <?php } ?>
			</div>
		</div>
	</section>
	<section class="section-service py-5">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="content-service-left d-flex justify-content-end align-items-center h-100" data-aos="fade">
						<div class="container text-center text-lg-start">
							<h1>Pilihan Metode Pelatihan</h1>
							<p>E-Learning Program, Distance Learning Program dan Face to Face Training.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-8">
					<div class="content-service-right">
						<div class="d-flex p-1" data-aos="zoom-in">
							<i class="fa fa-caret-right fs-1 flex-shrink-0 me-3 color-theme-1"></i>
							<div class="">
								<h2>E-Learning Program</h2>
								<p>Pembelajaran yang disusun dengan tujuan menggunakan sistem elektronik atau komputer sehingga mampu mendukung proses pembelajaran.</p>
							</div>
						</div>
						<div class="d-flex p-1" data-aos="zoom-in">
							<i class="fa fa-caret-right fs-1 flex-shrink-0 me-3 color-theme-1"></i>
							<div class="">
								<h2>Distance Learning Program</h2>
								<p>Pembelajaran yang peserta didik dan instrukturnya berada di lokasi terpisah sehingga memerlukan sistem telekomunikasi interaktif untuk menghubungkan keduanya.</p>
							</div>
						</div>
						<div class="d-flex p-1" data-aos="zoom-in">
							<i class="fa fa-caret-right fs-1 flex-shrink-0 me-3 color-theme-1"></i>
							<div class="">
								<h2>Face to Face Training</h2>
								<p>Pembelajaran yang dilakukan secara langsung atau tatap muka dalam ruang kelas.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bg-theme-1 py-5">
		<div class="container" data-aos="fade-right">
			<div class="d-block d-lg-flex justify-content-between text-center text-lg-left">
				<h1>Siap menjadi kompeten bersama kami?</h1>
				<a href="https://wa.me/62816343741" target="blank" class="btn btn-theme btn-outline-light">Hubungi Kami</a>
			</div>
		</div>
	</section>
	<section class="section-client py-5">
		<div class="container">
			<div class="heading mb-5 text-center" data-aos="fade">
				<h1>Klien yang sudah kami bantu</h1>
			</div>
			<div data-aos="fade-up">
				<div id="client-carousel" class="owl-carousel owl-theme">
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600316523-01-min.png"><p class="text-center">PersonalityTalk</p></div>
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600411288-01-min.png"><p class="text-center">LSP Teknologi Digital</p></div>
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600411334-01-min.png"><p class="text-center">Tempat Uji Kompetensi</p></div>
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600316223-01.png"><p class="text-center">LSP Perkopersian Indonesia</p></div>
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600316233-01.png"><p class="text-center">LSP Pelatinas</p></div>
				  <div class="item"><img class="p-4" src="<?php bloginfo('template_url'); ?>/assets/images/client/1600316243-01.png"><p class="text-center">LSP MPSDM</p></div>
				  <!-- <div class="item"><img class="p-4" src="<?php //bloginfo('template_url'); ?>/assets/images/client/download.jpg"><p class="text-center">LSP Pariwisata Gunadarma</p></div> -->
				</div>
			</div>
	</section>
	<section class="secttion-article py-5 bg-light">
		<div class="container">
			<div class="heading mb-5 text-center" data-aos="fade">
				<h1 class="text-uppercase">Artikel Terbaru</h1>
			</div>
			<div class="content-articel">
				<div class="row my-5">

					<?php

					// limit post 
		            $args = array( 
		                'post_type' => 'post', 
		                'posts_per_page' => 6,
		                'cat' => '2', 
		            );
					$query = new WP_Query( $args ); //query the categories

					if ($query->have_posts()) {
					while ($query->have_posts()) {
					$query->the_post(); ?>

				    <div class="col-12 col-md-6 col-lg-4">
				      <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
				      <div class="card border-0 shadow-sm rounded-2 mb-4" data-aos="fade-up">
						<a class="text-decoration-none text-body" href="<?php the_permalink();?>">
							<!-- no blank thumbnail -->
							<?php if ( has_post_thumbnail() ) { ?>
							<img class="card-img-top shadow rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
							style="background: url('<?php echo $url ?>'); width: 100%; height: 200px; background-position: center; background-repeat: no-repeat; background-size: cover;">
							<?php } else { ?>
							<img class="card-img-top shadow rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
							style="background: url('<?php bloginfo('template_directory'); ?>/assets/images/placeholder_600x400.svg'); width: 100%; height: 200px; background-position: center; background-repeat: no-repeat; background-size: cover;">
							<?php } ?>
						</a>
						<div class="card-body pb-0">
						    <!-- title -->
						    <a class="text-decoration-none text-body text-capitalize" href="<?php the_permalink();?>">
						      <p class="mb-0"><b>
						        <?php
						          $thetitle = $post->post_title;
						          $getlength = strlen($thetitle);
						          $thelength = 40;
						          echo substr($thetitle, 0, $thelength);
						          if ($getlength > $thelength) echo ". . .";
						        ?>
						      </b></p>
						    </a>
						    <!-- content -->
						    <p>
						      <hp class="post-subtitle">
						        <?php 
						        $excerpt= get_the_excerpt();
						        echo substr($excerpt, 0, 60).' . . .';?>
						      </hp>
						    </p>
						    <hr class="m-0">
						</div>
						<div class="card-footer bg-white border-0 pt-1 rounded-2">
							<div class="d-flex justify-content-between">
						    	<!-- author -->
						    	<div><i class="fa fa-user" aria-hidden="true"></i> <span class="text-capitalize"><?php the_author();?></span></div>
							    <!-- time -->
							    <div><i class="fa fa-clock" aria-hidden="true"></i> <span class="text-capitalize"><?php the_time();?></span></div>
						    </div>
						</div>
				      </div>
				    </div>
			        <?php } ?>
			        <?php }  else {
			            // not found article ?>
			            <div class="post-preview">
			                <h2 class="post-title">Article Not Found!!!</h2>
			            </div>
			        <?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php get_footer();?>