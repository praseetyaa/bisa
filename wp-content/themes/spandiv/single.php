<?php
get_header();
if (have_posts()) {
    while (have_posts()) {
        the_post();
        /**
         * menggunakan featured image
         */
        if (has_post_thumbnail()) {
            $thumbnail = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
        } else {
            $thumbnail = get_template_directory_uri() . '/assets/images/businessman-showing-changes-report_1098-3504.jpg';
        }
        ?>


    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
<section class="section-header">
    <div class="container-header">
        <div class="bg-image">
            <img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
            <div class="mask" style="background-color: rgba(0,0,0,.6);">
                <div class="d-flex justify-content-end align-items-end text-white h-100">
                    <div class="container">
                        <h1 class="text-capitalize"><?php the_title();?></h1>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item text-capitalize" aria-current="page"><a href="<?php bloginfo('url'); ?>/category/<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>/"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php the_title();?></li>
                          </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    body{background-color: #f8f9fa}
    .img-overlay{
    background: url(<?php echo get_template_directory_uri() .'/assets/images/photo-1517048676732-d65bc937f952.jpg';?>);
    height: 200px;
    width: 100%;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;}
    .breadcrumb-item a{color: rgba(255,255,255,1); text-decoration: none;}
    .breadcrumb-item.active{color: rgba(255,255,255,.55);}
</style>
<div class="container">
    <div class="row berita my-5">
        <!-- Post Content -->
        <div class="col-12 col-lg-8">
            <article>
                <div class="entry">
                    <!-- no blank thumbnail -->
                    <?php if ( has_post_thumbnail() ) { ?>
                    <img class="me-2 mb-3 rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
                    style="background: url('<?php the_post_thumbnail_url() ?>'); width: 100%; height: 400px; background-position: center; background-repeat: no-repeat; background-size: cover;">
                    <?php } else { ?>
                    <img class="me-2 mb-3 rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
                    style="background: url('<?php bloginfo('template_directory'); ?>/assets/images/placeholder_600x400.svg'); width: 100%; height: 400px;; background-position: center; background-repeat: no-repeat; background-size: cover;">
                    <?php } ?>
                    <!-- title -->
                    <h3 class="text-capitalize"><?php the_title();?></h3>
                    <!-- meta -->
                    <span class="meta text-muted">
                        <!-- author -->
                        <i class="fa fa-user" aria-hidden="true"></i> 
                        <?php the_author();?>
                        <!-- time -->
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <?php the_date( 'D F Y' ); ?>
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <?php the_time(); ?>
                        <!-- tag -->
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <?php
                        $posttags = get_the_tags();
                        $count=0;
                        if ($posttags) {
                          foreach($posttags as $tag) {
                            $count++;
                            if (1 == $count) {
                              echo $tag->name;
                            }
                          }
                        }
                        ?>
                    </span>
                    <!-- content -->
                    <?php
                        // show the content without image
                        add_filter('the_content', 'strip_images',2);
                        function strip_images($content){
                           return preg_replace('/<img[^>]+./','',$content);
                        }
                        // show the content
                        the_content();
                    ?>
                </div>
                <div class="comment">
                    <?php comments_template();?>
                </div>
            </article>
        </div>
        <?php include get_theme_file_path( '/partials/sidebar.php' ); ?>
    </div>
</div>


        <?php
    }
}
?>
<?php get_footer();?>