<section class="section-header">
    <div class="container-header">
        <div class="bg-image">
            <img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
            <div class="mask" style="background-color: rgba(0,0,0,.6);">
                <div class="d-flex justify-content-end align-items-center text-white h-100">
                    <div class="container">
                        <h1>
			            	<?php foreach((get_the_category()) as $category){
			                    echo $category->name;
			                    echo category_description($category);}
			            	?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
	.img-overlay{
	background: url(<?php echo get_template_directory_uri() .'/assets/images/photo-1517048676732-d65bc937f952.jpg';?>);
	height: 300px;
	width: 100%;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;}
</style>