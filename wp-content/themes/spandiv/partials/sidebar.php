<div class="col-12 col-lg-4">
	<div class="card border-0 bg-white shadow-sm mb-4 rounded-2">
		<div class="card-header bg-transparent mx-3 px-0">
			<h4>Kategori</h4>
		</div>
		<div class="card-body">
			<div class="">
			<?php
			$categories = get_categories();
			foreach($categories as $category) {
				if ($category->term_id== 1 || $category->term_id== 2 || $category->term_id== 11) {
					echo '<div class="col pb-2"><a class="text-decoration-none link-dark fs-6" href="' . get_category_link($category->term_id) . '"><i class="fa fa-chevron-right me-2"></i>' . ucfirst($category->name) . '</a></div>';
				}
			} ?>
			</div>
		</div>
	</div>

	<div class="card border-0 bg-white shadow-sm mb-4 rounded-2">
		<div class="card-header bg-transparent mx-3 px-0">
		    <h4>Artikel Terbaru</h4>
		</div>
		<div class="card-body">

			<?php
			// limit post 
            $args = array( 
                'post_type' => 'post', 
                'posts_per_page' => 6,
                'cat' => '2', 
            );
			$query = new WP_Query( $args ); //query the categories

			if ($query->have_posts()) {
			while ($query->have_posts()) {
			$query->the_post(); ?>

			<div class="d-flex my-2">
				<a class="text-decoration-none text-body" href="<?php the_permalink();?>">
					<!-- no blank thumbnail -->
					<?php if ( has_post_thumbnail() ) { ?>
					<img class="flex-shrink-0 me-2 rounded" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
					style="background: url('<?php the_post_thumbnail_url() ?>'); width: 70px; height: 70px; background-position: center; background-repeat: no-repeat; background-size: cover;">
					<?php } else { ?>
					<img class="flex-shrink-0 me-2 rounded" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
					style="background: url('<?php bloginfo('template_directory'); ?>/assets/images/placeholder_600x400.svg'); width: 70px; height: 70px;; background-position: center; background-repeat: no-repeat; background-size: cover;">
					<?php } ?>
					<div>
						<h5 class="link-dark">
						  <?php
						    $thetitle = $post->post_title;
						    $getlength = strlen($thetitle);
						    $thelength = 30;
						    echo substr($thetitle, 0, $thelength);
						    if ($getlength > $thelength) echo "...";
						  ?>
						</h5>
						<small class="text-muted mb-3">
							<i class="fa fa-user"></i> 
							<a class="text-decoration-none text-muted"><?php the_author();?></a>
							<i class="fa fa-clock"></i> <?php the_time(); ?>
						</small>
					</div>
				</a>
		  	</div>

	        <?php } ?>
            <?php }  else {
	            // not found article ?>
	            <div class="post-preview">
	                <h2 class="post-title">Article Not Found!!!</h2>
	            </div>
	        <?php } ?>
		</div>
	</div>
	<div class="card border-0 bg-white shadow-sm shadow-sm rounded-2">
		<div class="card-header bg-transparent mx-3 px-0">
			<h4>Tag</h4>
		</div>
		<div class="card-body">
			<div class="">
				<?php
				$posttags = get_the_tags();
				if ($posttags) {
				  foreach($posttags as $tag) {
				    echo '<span class="badge bg-dark text-capitalize">' . $tag->name . ' ' . '</span>'; 
				  }
				}
				?>
			</div>
		</div>
	</div>
</div>