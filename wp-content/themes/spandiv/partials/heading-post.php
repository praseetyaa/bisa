<section class="section-header">
    <div class="container-header">
        <div class="bg-image">
            <img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
            <div class="mask" style="background-color: rgba(0,0,0,.6);">
                <div class="d-flex justify-content-end align-items-center text-white h-100">
                    <div class="container">
                        <h1 class="text-capitalize"><?php the_title();?></h1>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Artikel</li>
                          </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
	.img-overlay{
	background: url(<?php echo get_template_directory_uri() .'/assets/images/photo-1517048676732-d65bc937f952.jpg';?>);
	height: 300px;
	width: 100%;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;}
    .breadcrumb-item a{color: rgba(255,255,255,1); text-decoration: none;}
    .breadcrumb-item.active{color: rgba(255,255,255,.55);}
</style>