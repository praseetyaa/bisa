<?php
/*
Template Name: Program Page
*/
get_header(); ?>
<section class="section-header">
    <div class="container-header">
        <div class="bg-image">
            <img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
            <div class="mask" style="background-color: rgba(0,0,0,.6);">
                <div class="d-flex justify-content-end align-items-end text-white h-100">
                    <div class="container">
                        <h1 class="text-capitalize"><?php the_title();?></h1>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Program</li>
                          </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    body{background-color: #f8f9fa}
    .img-overlay{
    background: url(<?php echo get_template_directory_uri() .'/assets/images/photo-1517048676732-d65bc937f952.jpg';?>);
    height: 200px;
    width: 100%;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;}
    .breadcrumb-item a{color: rgba(255,255,255,1); text-decoration: none;}
    .breadcrumb-item.active{color: rgba(255,255,255,.55);}
</style>
<div class="container">
    <div class="my-5">

        <?php
        // limit post 
        $args = array( 
            'post_type' => 'post', 
            'posts_per_page' => 6,
            'cat' => '10', 
            'order'   => 'ASC',
        );
        $query = new WP_Query( $args ); //query the categories

        if ($query->have_posts()) {
        while ($query->have_posts()) {
        $query->the_post(); ?>

        <div class="card border-0 shadow-sm rounded-2 my-4">
            <div class="card-header bg-theme-1 d-flex align-items-baseline py-3 rounded-2 shadow">
                <i class="fa fa-chevron-right me-2 fs-3"></i>
                <h3 class="mb-0"><?php echo the_title(); ?></h3>
            </div>
            <div class="card-body">
                <p><?php echo the_content(); ?></p>
            </div>
        </div>

        <?php } ?>
        <?php }  else {
            // not found article ?>
            <div class="post-preview">
                <h2 class="post-title">Article Not Found!!!</h2>
            </div>
        <?php } ?>
    </div>
</div>



<?php get_footer(); ?>