<?php
/**
* A Simple Category Template
*/
 
get_header(); ?> 
<section class="section-header">
    <div class="container-header">
        <div class="bg-image">
            <img class="img-overlay" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
            <div class="mask" style="background-color: rgba(0,0,0,.6);">
                <div class="d-flex justify-content-end align-items-end text-white h-100">
                    <div class="container">
                        <h1 class="text-capitalize"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></h1>
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active text-capitalize" aria-current="page"><?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></li>
                          </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
	body{background-color: #f8f9fa}
    .img-overlay{
    background: url(<?php echo get_template_directory_uri() .'/assets/images/photo-1517048676732-d65bc937f952.jpg';?>);
    height: 200px;
    width: 100%;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;}
    .breadcrumb-item a{color: rgba(255,255,255,1); text-decoration: none;}
    .breadcrumb-item.active{color: rgba(255,255,255,.55);}
</style>
<div class="container" id="primary" class="site-content">

	<div class="row my-5">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		    <div class="col-12 col-md-6 col-lg-4">
		      <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
		      <figure>
		      <div class="card border-0 shadow-sm rounded-2">
				<a class="text-decoration-none text-body" href="<?php the_permalink();?>">
					<!-- no blank thumbnail -->
					<?php if ( has_post_thumbnail() ) { ?>
					<img class="card-img-top shadow rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
					style="background: url('<?php echo $url ?>'); width: 100%; height: 200px; background-position: center; background-repeat: no-repeat; background-size: cover;">
					<?php } else { ?>
					<img class="card-img-top shadow rounded-2" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Generic" placeholder="image"
					style="background: url('<?php bloginfo('template_directory'); ?>/assets/images/placeholder_600x400.svg'); width: 100%; height: 200px; background-position: center; background-repeat: no-repeat; background-size: cover;">
					<?php } ?>
				</a>
				<div class="card-body pb-0">
				    <!-- title -->
				    <a class="text-decoration-none text-body text-capitalize" href="<?php the_permalink();?>">
				      <p class="mb-0"><b>
				        <?php
				          $thetitle = $post->post_title;
				          $getlength = strlen($thetitle);
				          $thelength = 40;
				          echo substr($thetitle, 0, $thelength);
				          if ($getlength > $thelength) echo ". . .";
				        ?>
				      </b></p>
				    </a>
				    <!-- content -->
				    <p>
				      <hp class="post-subtitle">
				        <?php 
				        $excerpt= get_the_excerpt();
				        echo substr($excerpt, 0, 60).' . . .';?>
				      </hp>
				    </p>
				    <hr class="m-0">
				</div>
				<div class="card-footer bg-white border-0 pt-1 rounded-2">
					<div class="d-flex justify-content-between">
				    	<!-- author -->
				    	<div><i class="fa fa-user" aria-hidden="true"></i> <span class="text-capitalize"><?php the_author();?></span></div>
					    <!-- time -->
					    <div><i class="fa fa-clock" aria-hidden="true"></i> <span class="text-capitalize"><?php the_time();?></span></div>
				    </div>
				</div>
		      </div>
		    </figure>
		    </div>
		 
		<?php endwhile; 
		 
		else: ?>
		<div class="container">
			<img src="https://i.imgur.com/QLyVDoK.gif">
		</div>
		 
		 
		<?php endif; ?>
	</div>

</div>

<?php get_footer(); ?>