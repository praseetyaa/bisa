	<footer class="section-footer bg-theme-1 pt-5">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-12 col-lg-3">
					<a href="#">
						<img class="mb-5 mb-lg-0" height="50" src="<?php bloginfo('template_url'); ?>/assets/images/logo/logo-1.png" style="filter: brightness(0) invert(1);">
					</a>
				</div>
				<div class="col-12 col-md-3 col-lg-2">
					<ul class="list-group list-group-flush">
						<li class="list-group-item bg-transparent border-0 px-0 fw-bold">
							Menu
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<a class="footer-link text-decoration-none" href="<?php bloginfo('url'); ?>/tentang-kami/">Tentang Kami</a>
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<a class="footer-link text-decoration-none" href="<?php bloginfo('url'); ?>/program/">Program</a>
						</li>
					</ul>
				</div>
				<div class="col-12 col-md-3 col-lg-2">
					<ul class="list-group list-group-flush">
						<li class="list-group-item bg-transparent border-0 px-0 fw-bold">
							Kategori
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<a class="footer-link text-decoration-none" href="<?php bloginfo('url'); ?>/category/artikel/">Artkel</a>
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<a class="footer-link text-decoration-none" href="<?php bloginfo('url'); ?>/category/acara/">Acara</a>
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<a class="footer-link text-decoration-none" href="<?php bloginfo('url'); ?>/category/tak-berkategori/">Tak Berkategori</a>
						</li>
					</ul>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<ul class="list-group list-group-flush">
						<li class="list-group-item bg-transparent border-0 px-0 fw-bold">
							Hubungi Kami
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<i class="fa fa-map-marker-alt position-absolute" style="font-size: 22px"></i>
							<p class="mb-0" style="margin-left: 2em"> Jl Kedungmundu Raya 27 B Kota Semarang</p>
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<i class="fab fa-whatsapp position-absolute" style="font-size: 22px"></i>
							<p class="mb-0" style="margin-left: 2em"> +62816343741</p>
						</li>
						<li class="list-group-item bg-transparent border-0 px-0">
							<i class="fa fa-envelope position-absolute" style="font-size: 22px"></i>
							<p class="mb-0" style="margin-left: 2em"> info@bisa.co.id</p>
						</li>
					</ul>					
				</div>
			</div>
		</div>
		<div class="copyright text-center mt-5 py-2" style="background-color: rgba(0,0,0,.2);">
			<span>Copyright ©2021 BISA Indonesia. All rights reserved</span>
		</div>
	</footer>

    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQ -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- aos -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
  		AOS.init();
	</script>
    <!-- owl -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			$(window).scroll(function() {    
			    var scroll = $(window).scrollTop();

			     //>=, not <=
			    if (scroll >= 100) {
			        //clearHeader, not clearheader - caps H
			        $(".navbar").addClass("shrink");
			    } else {
			      $(".navbar").removeClass("shrink");
			    }
			});
		});
    </script>
    <script type="text/javascript">
		jQuery(document).ready(function($) {
		"use strict";
		$('#client-carousel').owlCarousel( {
				loop: true,
				center: true,
				items: 5,
				margin: 30,
				autoplay: true,
				autoplayTimeout: 2000,
				responsive: {
					0: {
						items: 1
					},
					768: {
						items: 3
					},
					1170: {
						items: 5
					}
				}
			});
		});
    </script>
	<!-- swipe start -->
	<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
	<script type="text/javascript">
	    jQuery(".carousel").swipe({
	        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
	            if (direction == 'left') jQuery(this).carousel('next');
	            if (direction == 'right') jQuery(this).carousel('prev');
	        },
	        allowPageScroll: "vertical" 
	    });
	</script>
	<!-- swipe end -->
	<script type="text/javascript">
	var myCarousel = document.querySelector('#carouselExampleFade')
	var carousel = new bootstrap.Carousel(myCarousel, {
	  interval: 6000,
	  pause: 'hover',
	  keyboard: true,
	})
	</script>

    <?php wp_footer(); ?>
    </body>
</html>




	<!-- - pusat pelatihan dan sertifikasi profesi nasional
	- unisbank mgmp
	logo lsp pelatinas lsp mpsdm lsp perkopersian indonesia lsp pariwisata gunadarma teknologi digital

	- tambah wa floating

	//slider
	bisnis adalah tentang membangun SDM 
	mengapa kita perlu mengembangkan SDM untuk menumbuhkan bisnis?
	Masukkan Email Anda Di Bawah Ini Untuk Mempelajari Alasannya!
	email input() -->