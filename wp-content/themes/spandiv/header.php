<!DOCTYPE html>
 <html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="shortcut icon" type="image/x-icon" href="https://kompetensiku.id/assets/images/logo/1603348880-icon.png">
		<title><?php echo get_bloginfo( 'name' ) . ' | '. get_bloginfo( 'description' ); ?></title>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/all.css" type="text/css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/bootstrap/css/bootstrap.min.css" type="text/css" />
		<link href="https://fonts.googleapis.com/css2?family=Satisfy&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" />

		<?php wp_head(); ?>
	</head>

   <body <?php body_class('spandiv'); ?>>

	<nav class="navbar navbar-expand-lg navbar-light fixed-top">
	  <div class="container">
		<a class="navbar-brand" href="<?php echo home_url() ?>">
			<img height="50" src="<?php bloginfo('template_url'); ?>/assets/images/logo/logo-1.png">
		</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="nav-link  <?= is_front_page() ? 'active' : '' ?> " href="<?php echo home_url() ?>">Home</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link <?= is_page('tentang-kami') ? 'active' : '' ?> " href="<?php bloginfo('url'); ?>/tentang-kami/">Tentang Kami</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link <?= is_page('program') ? 'active' : '' ?> " href="<?php bloginfo('url'); ?>/program/">Program</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link <?= is_int(strpos($_SERVER['REQUEST_URI'], '/category/artikel'))  ? 'active' : '' ?> " href="<?php bloginfo('url'); ?>/category/artikel/">Artikel</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link <?php echo is_int(strpos($_SERVER['REQUEST_URI'], '/category/acara')) ? 'active' : '' ?> " href="<?php bloginfo('url'); ?>/category/acara/">Acara</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</nav>
	<!-- fab -->
	<div class="fixed-bottom d-flex align-items-center justify-content-end text-end">
		<div class="bg-white shadow-sm px-3 py-2 me-2 rounded-3" style="width: fit-content; animation: fab 2s infinite ease">
			<span class="fw-bold">Contact Us</span>
		</div>
		<a href="https://wa.me/62816343741" target="blank">
		<div class="rounded-circle shadow-sm float-end text-center d-flex align-items-center justify-content-center me-2 mb-2" style="width: 60px; height: 60px; background-color: #00E676;">
			<i class="fab fa-whatsapp fs-1 text-white"></i>
		</div>
		</a>
	</div>